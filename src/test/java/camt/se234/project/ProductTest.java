package camt.se234.project;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.service.ProductServiceImpl;
import org.hamcrest.core.IsNot;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductTest {
    ProductDao productDao;
    ProductServiceImpl productService;
    @Before
    public void setup(){
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void testGetAllProduct(){
        List<Product> mockProduct = new ArrayList<>();
        mockProduct.add(new Product(1l,"Item_id_1","Item_name_1","Item_des_1","Item_image_1",999.00));
        mockProduct.add(new Product(2l,"Item_id_2","Item_name_2","Item_des_2","Item_image_2",111.00));
        mockProduct.add(new Product(3l,"Item_id_3","Item_name_3","Item_des_3","Item_image_3",123.45));
        mockProduct.add(new Product(4l,"Item_id_4","Item_name_4","Item_des_4","Item_image_4",456.78));
        mockProduct.add(Product.builder()
                .id(999l)
                .productId("Item_id_5")
                .name("Item_name_5")
                .description("Item_des_5")
                .imageLocation("Item_image_5")
                .price(1234.6)
                .build());
        when(productDao.getProducts()).thenReturn(mockProduct);
        assertThat(productService.getAllProducts(),is((mockProduct)));
    }

    @Test
    public void TestgetAvailableProducts(){
        List<Product> products = new ArrayList<>();
        products.add(new Product(1l,"Item_id_1","Item_name_1","Item_des_1","Item_image_1",999.00));
        products.add(new Product(2l,"Item_id_2","Item_name_2","Item_des_2","Item_image_2",111.00));
        products.add(new Product(3l,"Item_id_3","Item_name_3","Item_des_3","Item_image_3",123.45));
        products.add(new Product(4l,"Item_id_4","Item_name_4","Item_des_4","Item_image_4",456.78));
        products.add(Product.builder()
                .id(999l)
                .productId("Item_id_5")
                .name("Item_name_5")
                .description("Item_des_5")
                .imageLocation("Item_image_5")
                .price(-1)
                .build());


        Product notused = new Product(999l,"Item_id_5","Item_name_5","Item_des_5","Item_image_5",-1);
        when(productDao.getProducts()).thenReturn(products);
        assertThat(productService.getAvailableProducts(), new IsNot(hasItem(notused)));

    }

    @Test
    public void TestgetUnavailableProductSize(){
        List<Product> products = new ArrayList<>();
        products.add(new Product(1l,"Item_id_1","Item_name_1","Item_des_1","Item_image_1",999.00));
        products.add(new Product(2l,"Item_id_2","Item_name_2","Item_des_2","Item_image_2",111.00));
        products.add(new Product(3l,"Item_id_3","Item_name_3","Item_des_3","Item_image_3",123.45));
        products.add(new Product(4l,"Item_id_4","Item_name_4","Item_des_4","Item_image_4",456.78));
        products.add(Product.builder()
                .id(999l)
                .productId("Item_id_5")
                .name("Item_name_5")
                .description("Item_des_5")
                .imageLocation("Item_image_5")
                .price(-1)
                .build());
        products.add(Product.builder()
                .id(999l)
                .productId("Item_id_6")
                .name("Item_name_6")
                .description("Item_des_6")
                .imageLocation("Item_image_6")
                .price(-222)
                .build());

        when(productDao.getProducts()).thenReturn(products);
        assertThat(productService.getUnavailableProductSize(),is(2));
    }


}
