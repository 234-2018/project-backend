package camt.se234.project;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;
    @Before
    public void setup(){
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);
    }

    @Test
    public void TestGetSaleOrder(){
        List<SaleOrder> saleOrderList = new ArrayList<>();
        List<SaleTransaction> saleTransactionList = new ArrayList<>();
        saleTransactionList.add(SaleTransaction.builder()
                .id(234l)
                .transactionId("transaction1")
                .amount(2)
                .product(Product.builder()
                        .id(1l)
                        .price(945.00)
                        .imageLocation("ImageLocation1")
                        .description("Itemdescription1")
                        .name("iteemname1")
                        .productId("productId1")
                        .build())
                .build());

        saleOrderList.add(SaleOrder.builder()
                .id(123l)
                .saleOrderId("order1")
                .transactions(saleTransactionList)
                .build());
        when(orderDao.getOrders()).thenReturn(saleOrderList);
        assertThat(saleOrderService.getSaleOrders(),is(saleOrderList));
    }


    @Test
    public void TestGetAverageSaleOrderPrice(){
        List<SaleOrder> saleOrderList = new ArrayList<>();
        List<SaleTransaction> saleTransactionList = new ArrayList<>();
        saleTransactionList.add(SaleTransaction.builder()
                .id(234l)
                .transactionId("transaction1")
                .amount(2)
                .product(Product.builder()
                        .id(1l)
                        .price(500)
                        .imageLocation("ImageLocation1")
                        .description("Itemdescription1")
                        .name("iteemname1")
                        .productId("productId1")
                        .build())
                .build());
        saleTransactionList.add(SaleTransaction.builder()
                .id(123l)
                .transactionId("transaction2")
                .amount(5)
                .product(Product.builder()
                        .id(1l)
                        .price(100)
                        .imageLocation("ImageLocation2")
                        .description("Itemdescription2")
                        .name("iteemname2")
                        .productId("productId2")
                        .build())
                .build());


        saleOrderList.add(SaleOrder.builder()
                .id(123l)
                .saleOrderId("order1")
                .transactions(saleTransactionList)
                .build());
        when(orderDao.getOrders()).thenReturn(saleOrderList);
        assertThat(saleOrderService.getAverageSaleOrderPrice(),is(1500.0));
    }

}
