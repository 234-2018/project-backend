package camt.se234.project;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationTest {
    UserDao userDao;
    AuthenticationServiceImpl authenticationService;
    @Before
    public void setup(){
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }


    @Test
    public void testExistwithMock(){
        User mockUser = new User(123l,"Admin","1234","user");
        when(userDao.getUser("Admin","1234")).thenReturn(mockUser);
        assertThat(authenticationService.authenticate("Admin","1234"),is(mockUser));
    }

    @Test
    public void testDoesnotExistwithMock(){
        when(userDao.getUser("Admin","1234")).thenReturn(null);
        assertThat(authenticationService.authenticate("Admin","1234"),nullValue());
    }

}